��          �      \      �     �     �     �       E   "  E   h     �     �     �       #     "   ?     b     j     s  I   w  I   �  8     (   D  d  m     �     �     �       E   #  E   i     �     �     �       #     "   @     c     k     t  I   x  I   �  8     (   E         	                                                                                  
        Add a new OLA Add a new SLA Automatic actions of OLA Automatic reminders of OLAs Escalations defined in the OLA will be triggered under this new date. Escalations defined in the SLA will be triggered under this new date. Internal time to own Internal time to own + Progress Internal time to own exceedeed Internal time to resolve Internal time to resolve + Progress Internal time to resolve exceedeed Locales SLA SLAs SLM The assignment of a OLA to a ticket causes the recalculation of the date. The assignment of a SLA to a ticket causes the recalculation of the date. The internal time is recalculated when assigning the OLA This plugin requires GLPI 0.85 or higher Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-04-07 13:28+0200
PO-Revision-Date: 2017-04-07 13:28+0200
Last-Translator: 
Language-Team: 
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
 Add a new OLA Add a new SLA Automatic actions of OLA Automatic reminders of OLAs Escalations defined in the OLA will be triggered under this new date. Escalations defined in the SLA will be triggered under this new date. Internal time to own Internal time to own + Progress Internal time to own exceedeed Internal time to resolve Internal time to resolve + Progress Internal time to resolve exceedeed Locales SLA SLAs SLM The assignment of a OLA to a ticket causes the recalculation of the date. The assignment of a SLA to a ticket causes the recalculation of the date. The internal time is recalculated when assigning the OLA This plugin requires GLPI 0.85 or higher 
��          �      \      �     �     �     �       E   "  E   h     �     �     �       #     "   ?     b     j     s  I   w  I   �  8     (   D  c  m     �     �     �       h   :  i   �        .   .  *   ]     �  *   �  &   �     �            `     `   ~  >   �  ,   	         	                                                                                  
        Add a new OLA Add a new SLA Automatic actions of OLA Automatic reminders of OLAs Escalations defined in the OLA will be triggered under this new date. Escalations defined in the SLA will be triggered under this new date. Internal time to own Internal time to own + Progress Internal time to own exceedeed Internal time to resolve Internal time to resolve + Progress Internal time to resolve exceedeed Locales SLA SLAs SLM The assignment of a OLA to a ticket causes the recalculation of the date. The assignment of a SLA to a ticket causes the recalculation of the date. The internal time is recalculated when assigning the OLA This plugin requires GLPI 0.85 or higher Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-04-07 13:26+0200
PO-Revision-Date: 2017-04-07 13:28+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajout d'un nouveau OLA Ajouter un nouveau SLA Actions automatiques des OLA Rappels automatiques des OLAs Les escalades définies dans l'OLA seront également déclenchées conformément à cette nouvelle date. Les escalades définies dans la SLA seront également déclenchées conformément à cette nouvelle date. Temps de prise en charge interne Temps de prise en charge interne + Progression Temps de prise en charge interne dépassé Temps de résolution interne Temps de résolution interne + Progression Temps de résolution interne dépassé Traduction pour les OLAs SLA SLAs SLM L'affectation d'une OLA à un ticket a posteriori entraîne le recalcul de la date d'échéance. L'affectation d'une SLA à un ticket a posteriori entraîne le recalcul de la date d'échéance. Le temps interne est recalculé lors de l'affectation de l'OLA Ce plugin nécessite GLPI 0.85 ou supérieur 
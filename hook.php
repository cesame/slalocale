<?php
/*
 -------------------------------------------------------------------------
 slalocale plugin for GLPI
 Copyright (C) 2014 by the slalocale Development Team.
 -------------------------------------------------------------------------

 LICENSE

 This file is part of slalocale.

 slalocale is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 slalocale is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with slalocale. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------  */

/**
 * @return bool
 */
function plugin_slalocale_install() {
   global $DB;

   return true;
}

/**
 * @return bool
 */
function plugin_slalocale_uninstall() {
   global $DB;


   return true;
}


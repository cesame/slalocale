<?php
/*
 -------------------------------------------------------------------------
 slalocale plugin for GLPI
 Copyright (C) 2014 by the slalocale Development Team.
 -------------------------------------------------------------------------

 LICENSE

 This file is part of slalocale.

 slalocale is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 slalocale is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with slalocale. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------  */

if (!defined('GLPI_ROOT')) {
   die("Sorry. You can't access directly to this file");
}

/**
 * Class PluginslalocaleLocale
 */
class PluginslalocaleLocale extends CommonDBTM {
   
   static $rightname = 'plugin_slalocale';

   function listLocales() {

      echo __('SLM', 'slalocale');
      echo _n('SLA', 'SLAs', 1, 'slalocale');
      echo __('Internal time to own + Progress', 'slalocale');
      echo __('Internal time to own', 'slalocale');
      echo __('Internal time to own exceedeed', 'slalocale');
      echo __('Internal time to resolve', 'slalocale');
      echo __('Internal time to resolve + Progress', 'slalocale');
      echo __('Internal time to resolve exceedeed', 'slalocale');
      echo __('Add a new OLA', 'slalocale');
      echo __('The assignment of a OLA to a ticket causes the recalculation of the date.', 'slalocale');
      echo __('Escalations defined in the OLA will be triggered under this new date.', 'slalocale');
      echo __('Add a new SLA', 'slalocale');
      echo __('Automatic reminders of OLAs', 'slalocale');
      echo __('The assignment of a SLA to a ticket causes the recalculation of the date.', 'slalocale');
      echo __('Escalations defined in the SLA will be triggered under this new date.', 'slalocale');
      echo __('Automatic actions of OLA', 'slalocale');
      echo __('The internal time is recalculated when assigning the OLA', 'slalocale');
   }
}

?>
